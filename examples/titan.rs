use clap::Parser;
use trotter::{Actor, Titan};

#[derive(Parser)]
struct Cli {
	url: String,
}

#[tokio::main]
async fn main() -> anyhow::Result<()> {
	let Cli { url } = Cli::parse();

	let g = Actor::default()
		.upload(
			url,
			Titan {
				content: "Example content :DDDDDDDDDDDDDDDDDDDD".into(),
				mimetype: "text/plain".into(),
				token: None,
			},
		)
		.await?
		.gemtext()?;
	println!("{g}");

	Ok(())
}

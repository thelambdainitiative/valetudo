use trotter::{Actor, UserAgent};

#[tokio::main]
async fn main() -> anyhow::Result<()> {
	Actor::default()
		.user_agent(UserAgent::Webproxy)
		.get("catboomer.net") // <- Points at a site that denies all robot traffic
		.await?;

	Ok(())
}

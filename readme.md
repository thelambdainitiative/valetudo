# 🎠 Trotter

Trotter is a fun and experimental gemini client crate.

Installing `trot` cli: `cargo install --features cli trotter`.

For simple requests, use [`trot`] and [`trot_in`].

``` rust,no_run
#[tokio::main]
async fn main() {
    trotter::trot("geminiprotocol.net") // gemini:// prefix and root slash can be elided
        .await
        .unwrap();

    trotter::trot_in("localhost/input", "notice me!")
        .await
        .unwrap();
}
```

## 😊 Requests

### 🎭 Actor

Actors store information relevant in multiple requests.

``` rust,no_run
use trotter::{Actor, UserAgent};

#[tokio::main]
async fn main() {
    let owo = Actor::default()
        .user_agent(UserAgent::Indexer)
        .cert_file("id/owo.crt")
        .key_file("id/owo.key");

    owo.get("localhost")
        .await
        .unwrap();
}
```

### 🤖 User Agent

Did you know there's a version of the `robots.txt` standard for gemini?
([robots.txt for Gemini](https://geminiprotocol.net/docs/companion/robots.gmi))

Trotter has robots functionality built-in. Once you set your user-agent, you
will receive a `RobotDenied` error if you try to access a page you are
disallowed from.

I strongly suggest you do this if you're using Trotter for a project that
depends on other peoples' content.

### 🌕 Titan

[Titan](https://communitywiki.org/wiki/Titan) is gemini's sister-protocol for uploading files.

To use titan, create a [`Titan`] struct and pass it to [`Actor::upload`].

``` rust,no_run
use trotter::{Actor, Titan};

#[tokio::main]
async fn main() -> anyhow::Result<()> {
    let g = Actor::default()
        .upload(
            "titan://localhost/",
            Titan {
                content:  "Example content :DDDDDDDDDDDDDDDDDDDD".into(),
                mimetype: "text/plain".into(),
                token:    None,
            },
        )
        .await?
        .gemtext()?;
    println!("{g}");

    Ok(())
}
```

## 📖 Parse

``` rust
use trotter::parse::Gemtext;

fn main() {
    let txt = "# 💎
## Is
### So
> effing
* dope
man
=> /path/to/somewhere i can take u there
``` alt text goes here
Here's a table
| The | Best |
|-----|------|
| 😘  | 😪   |
```";

    let gemtext = Gemtext::parse(txt);

    println!("{gemtext:#?}");
}
```

## Tips

### Certificates

With `openssl` installed, you can use the following shell functions to generate
and inspect x509 certificates.

``` sh
certgen() { [ -f "${1:?usage: certgen [domain]}.key" ] || [ -f "$1.crt" ] || ( openssl req -new -subj "/CN=$1" -x509 -newkey ec -pkeyopt ec_paramgen_curve:prime256v1 -days 3650 -nodes -out "$1.crt" -keyout "$1.key" && printf '📜 Cert generated\n' ) ;}
certinfo(){ openssl x509 -noout -text < "${1:?usage: certinfo [file]}" ;}
```

## Todo

- [ ] Add --generate-cert option to cli.
- [ ] Fix broken robots.txt :(
- [ ] Tofu store directory
- [ ] Byte read/write timeout
